﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        #region Singleton
        public static UIManager Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        [Header("Pages")]
        [SerializeField] private GameObject _pageWin;
        [SerializeField] private GameObject _pageLose;

        private void Start()
        {
            GameController.GameController.Instance.OnGameStateChanged += OnGameStateChanged;
            OnGameStateChanged(GameController.GameController.Instance.GameState);
        }

        public void GoToMenu(string sceneName)
        {
            if (GameController.GameController.Instance.PlayerScore > PlayerPrefs.GetInt("PlayerRecord", 0))
            {
                PlayerPrefs.SetInt("PlayerRecord", GameController.GameController.Instance.PlayerScore);
            }

            SceneManager.LoadScene(sceneName);
        }

        private void OnGameStateChanged(GameController.GameState gameState)
        {
            Cursor.visible = true;
            switch (gameState)
            {
                case GameController.GameState.GamePlay:
                    _pageWin.SetActive(false);
                    Cursor.visible = false;
                    _pageLose.SetActive(false);
                    break;
                case GameController.GameState.WaitingForInput:
                    break;
                case GameController.GameState.Finish:
                    break;
                case GameController.GameState.Win:
                    _pageWin.SetActive(true);
                    break;
                case GameController.GameState.Lose:
                    _pageLose.SetActive(true);
                    break;
            }
        }

        private void OnDestroy()
        {
            GameController.GameController.Instance.OnGameStateChanged -= OnGameStateChanged;
        }
    }
}
