﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class Leaderboard : MonoBehaviour
    {
        [SerializeField] private Text _playerName;
        [SerializeField] private Text _playerScore;
        [SerializeField] private Text _playerRank;

        public void InitBoard(string playerName, int playerScore, int playerRank)
        {
            _playerName.text = playerName;
            _playerScore.text = playerScore.ToString();
            _playerRank.text = "#" + (playerRank + 1).ToString();
        }
    }
}
