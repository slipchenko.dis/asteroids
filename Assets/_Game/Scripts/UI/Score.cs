﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Text))]
    public class Score : MonoBehaviour
    {
        private Text _scoreText;

        private void Start()
        {
            _scoreText = GetComponent<Text>();

            GameController.GameController.Instance.OnPlayerScoreChanged += SetScore;
            SetScore(GameController.GameController.Instance.PlayerScore);
        }

        private void SetScore(int score)
        {
            _scoreText.text = "SCORE: " + score.ToString();
        }
    }
}
