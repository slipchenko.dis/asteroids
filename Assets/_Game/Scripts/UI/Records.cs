﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace UI
{
    [System.Serializable]
    class Board
    {
        public string Name;
        public int Score;

        public Board(string name, int score)
        {
            this.Name = name;
            this.Score = score;
        }
    }

    public class Records : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private Leaderboard _leaderboardTemplate;
        [SerializeField] private Transform _shopScrollView;
        [SerializeField] private List<Board> _boards;

        private List<Leaderboard> _leaderboardsList;

        private void Start()
        {
            _leaderboardsList = new List<Leaderboard>();

            Board playerBoard = new Board("Player", PlayerPrefs.GetInt("PlayerRecord", 0));

            _boards.Add(playerBoard);

            var sort = from s in _boards orderby s.Score descending select s;

            CreateLeaderboards(sort.ToList());
        }

        private void CreateLeaderboards(List<Board> sortBoards)
        {
            for (int i = 0; i < sortBoards.Count; i++)
            {
                Leaderboard newLeaderboard = Instantiate(_leaderboardTemplate, _shopScrollView);
                newLeaderboard.InitBoard(sortBoards[i].Name, sortBoards[i].Score, i);
                _leaderboardsList.Add(newLeaderboard);
            }
        }
    }
}
