﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class MenuButtonHandler : MonoBehaviour
    {
        #region Singleton
        public static MenuButtonHandler Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion


        [SerializeField] private Sprite _volumeImageOff;
        [SerializeField] private Sprite _volumeImageOn;
        [SerializeField] private Image _volumeBtn;

        private void Start()
        {
            UpdateVolumeSprite();
        }

        public void StartGame(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

        public void ToggleVolume()
        {
            if (GameController.VolumeHandler.Instance.Volume)
            {
                GameController.VolumeHandler.Instance.Volume = false;
            }
            else
            {
                GameController.VolumeHandler.Instance.Volume = true;
            }

            UpdateVolumeSprite();
        }

        private void UpdateVolumeSprite()
        {
            if (GameController.VolumeHandler.Instance.Volume)
            {
                _volumeBtn.sprite = _volumeImageOn;

            }
            else
            {
                _volumeBtn.sprite = _volumeImageOff;
            }
        }
    }
}
