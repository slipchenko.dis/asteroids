﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthBar : MonoBehaviour
    {
        #region Singleton
        public static HealthBar Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        public Image[] _hearts;
        public Sprite _fullHeart;
        public Sprite _emptyHeart;

        public void UpdateHealthBar(int health)
        {
            for (int i = 0; i < _hearts.Length; i++)
            {
                if (i < health)
                {
                    _hearts[i].sprite = _fullHeart;
                }
                else
                {
                    _hearts[i].sprite = _emptyHeart;
                }
            }
        }
    }
}
