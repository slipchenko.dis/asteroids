﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bullet
{
    public enum BulletType
    {
        EnemyBullet,
        PlayerBullet
    }

    [RequireComponent(typeof(Rigidbody2D))]
    public class Bullet : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private float _forwardSpeed = 20f;
        [SerializeField] private BulletType _bulletType;

        public BulletType BulletType
        {
            get
            {
                return _bulletType;
            }
        }

        private Rigidbody2D _rigidbody;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _rigidbody.velocity = transform.up * _forwardSpeed;

            GameController.VolumeHandler.Instance.VolumeLaser();
        }
    }
}
