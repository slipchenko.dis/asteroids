﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    public class SimpleEnemy : Enemy
    {
        private Vector2 _targetPos;

        void Start()
        {
            StartCoroutine(Shoot());
            _targetPos = transform.position;
        }

        void Update()
        {
            Move();
            RotateToTarget(_targetPos);
        }

        override protected void Move()
        {
            if (Vector2.Distance(_targetPos, transform.position) < 1f)
            {
                SetVelocity();
            }
        }


        private void SetVelocity()
        {
            _rigidbody.velocity = Vector2.zero;

            var randomPos = GetRandomPosition();

            var direction = (Vector2)transform.position - randomPos;

            _targetPos = GetRandomPosition();

            _rigidbody.velocity = direction.normalized * _forwardSpeed;
        }

        private Vector2 GetRandomPosition()
        {
            var screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

            var randomPos = new Vector2(Random.Range(0, screenBounds.x), Random.Range(0, screenBounds.y));

            return randomPos;
        }

        IEnumerator Shoot()
        {
            while (true)
            {
                yield return new WaitForSeconds(_shootCooldown);
                ReleaseShoot();
            }
        }
    }
}
