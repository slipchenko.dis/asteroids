﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    public abstract class Enemy : MonoBehaviour
    {
        [Header("OnDestroy")]
        [SerializeField] private int _scoreAmountPerDestroy;
        [SerializeField] protected GameObject _destroyEffect;

        [Header("Movement")]
        [SerializeField] protected float _forwardSpeed;
        [SerializeField] protected float _rotationSpeed;
        [SerializeField] protected float _shootCooldown;

        [Header("Shooting")]
        [SerializeField] private GameObject _bullet;
        [SerializeField] private Transform _bulletSpawnPosition;

        protected Rigidbody2D _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        protected void ReleaseShoot()
        {
            Instantiate(_bullet, _bulletSpawnPosition.position, _bulletSpawnPosition.rotation);
        }

        protected abstract void Move();

        protected void RotateToTarget(Vector3 target)
        {
            var vectorToTarget = target - transform.position;
            var angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + 90;
            var q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * _rotationSpeed);
        }

        protected void Destroy()
        {
            GameController.GameController.Instance.PlayerScore += _scoreAmountPerDestroy;

            GameController.VolumeHandler.Instance.VolumeExplosion();

            if (_destroyEffect)
            {
                Instantiate(_destroyEffect, transform.position, Quaternion.identity);
            }

            Destroy(gameObject);
        }

        private void OnEnable()
        {
            GameController.Spawner.Instance.AddEnemyToList(this);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            OnTrigger(collision);
        }

        protected void OnTrigger(Collider2D collision)
        {
            var bullet = collision.GetComponent<Bullet.Bullet>();

            if (bullet && bullet.BulletType == Bullet.BulletType.PlayerBullet)
            {
                Destroy(bullet);
                GameController.Spawner.Instance.RemoveEnemyFromList(this);
                Destroy();
            }
        }
    }
}
