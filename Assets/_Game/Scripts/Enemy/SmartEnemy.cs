﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Enemy
{
    public class SmartEnemy : Enemy
    {
        private int _playerLayer = 1 << 8;
        private float _cooldownTimer = 0f;
        private float _shootDistance = 2f;

        private Transform _target;

        void Start()
        {
            _target = GameObject.FindObjectOfType<Player.PlayerMovement>().gameObject.transform;

            CalculateShootDistanceOnLevel(GameController.GameController.Instance.Level);
        }

        void Update()
        {
            Move();
            RotateToTarget(_target.position);
            ReleaseRay();
        }

        private void ReleaseRay()
        {
            RaycastHit2D maskHit = Physics2D.Raycast(transform.position, -transform.up, _shootDistance, _playerLayer);
            Debug.DrawRay(transform.position, -transform.up * _shootDistance, Color.magenta);

            if (maskHit.transform != null && _cooldownTimer <= 0)
            {
                _cooldownTimer = _shootCooldown;
                Cooldown();
                ReleaseShoot();
            }
        }

        private void CalculateShootDistanceOnLevel(int level)
        {
            _shootDistance = Mathf.Clamp(_shootDistance + level / 2, 2, 20);
        }

        private void Cooldown()
        {
            DOVirtual.Float(_cooldownTimer, 0, _shootCooldown, value => { _cooldownTimer = value; });
        }

        override protected void Move()
        {
            if (Vector2.Distance(transform.position, _target.position) <= _shootDistance)
            {
                return;
            }

            var posToMove = Vector2.MoveTowards(transform.position, _target.position, _forwardSpeed * Time.deltaTime);

            transform.position = new Vector3(posToMove.x, posToMove.y, 0);
        }
    }
}
