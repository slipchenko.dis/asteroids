﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerShooting : MonoBehaviour
    {
        [SerializeField] private GameObject _bullet;
        [SerializeField] private Transform _bulletSpawnPosition;

        private bool _shooting;

        private void Start()
        {
            GameController.GameController.Instance.OnGameStateChanged += OnGameStateChanged;
            OnGameStateChanged(GameController.GameController.Instance.GameState);
        }

        public void Shoot()
        {
            if (_shooting)
            {
                Instantiate(_bullet, _bulletSpawnPosition.position, _bulletSpawnPosition.rotation);
            }
        }

        private void OnGameStateChanged(GameController.GameState gameState)
        {
            if (gameState >= GameController.GameState.Finish)
            {
                _shooting = false;
            }
            else
            {
                _shooting = true;
            }
        }

        private void OnDestroy()
        {
            GameController.GameController.Instance.OnGameStateChanged -= OnGameStateChanged;
        }
    }
}
