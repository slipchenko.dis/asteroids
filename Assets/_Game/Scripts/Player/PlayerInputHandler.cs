﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PlayerInputHandler : MonoBehaviour
    {
        private PlayerMovement _playerMovement;
        private PlayerShooting _playerShooting;

        private void Start()
        {
            _playerMovement = GetComponent<PlayerMovement>();
            _playerShooting = GetComponent<PlayerShooting>();
        }

        private void Update()
        {
            var inputVector = Vector2.zero;

            inputVector.y = Input.GetAxis("Vertical");
            inputVector.x = Input.GetAxis("Horizontal");

            _playerMovement.SetInputVector(inputVector);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                _playerShooting.Shoot();
            }
        }
    }
}
