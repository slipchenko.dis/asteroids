﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMovement : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private float _forwardAcceleration;
        [SerializeField] private float _turnAcceleration;
        [SerializeField] private float _maxSpeed;

        private float _accelerationInput;
        private float _steeringInput;

        private float _rotationAngle = 0f;

        private Rigidbody2D _rigidbody;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            Move();
            Rotation();
        }

        private void Move()
        {
            var engineForceVector = (Vector2)transform.up * _accelerationInput * _forwardAcceleration;

            _rigidbody.AddForce(engineForceVector, ForceMode2D.Force);
        }

        private void Rotation()
        {
            _rotationAngle -= _steeringInput * _turnAcceleration;

            _rigidbody.MoveRotation(_rotationAngle);
        }

        public void SetInputVector(Vector2 inputVector)
        {
            _accelerationInput = inputVector.y;
            _steeringInput = inputVector.x;
        }
    }
}
