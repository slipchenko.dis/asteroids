﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Player
{
    public class PlayerHealth : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private int _health;
        [SerializeField] private float _immortalDuration;
        [SerializeField] private SpriteRenderer _playerSkin;

        private bool _finishImmortal, _immortal;

        private void Start()
        {
            GameController.GameController.Instance.OnGameStateChanged += OnGameStateChanged;
            OnGameStateChanged(GameController.GameController.Instance.GameState);

            ImmortalByTime(_immortalDuration);
        }

        private bool TakeDamage(int damage)
        {
            if (_finishImmortal || _immortal)
            {
                return false;
            }

            _health -= damage;

            if (_health <= 0)
            {
                gameObject.SetActive(false);
                GameController.GameController.Instance.GameState = GameController.GameState.Lose;
            }

            UI.HealthBar.Instance.UpdateHealthBar(_health);

            return true;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            var asteroid = collision.GetComponent<Asteroids.Asteroid>();

            if (asteroid)
            {
                if (TakeDamage(1))
                {
                    ImmortalByTime(_immortalDuration);
                    GameController.CameraController.Instance.Shake();
                }
            }

            var bullet = collision.GetComponent<Bullet.Bullet>();

            if (bullet && bullet.BulletType == Bullet.BulletType.EnemyBullet)
            {
                if (TakeDamage(1))
                {
                    ImmortalByTime(_immortalDuration);
                }
            }
        }

        private void ImmortalByTime(float immortalDurration)
        {
            _playerSkin.DOFade(0, 0);
            _immortal = true;

            _playerSkin.DOFade(1, immortalDurration).SetEase(Ease.InOutFlash).OnComplete(() =>
            {
                _playerSkin.DOFade(1, 0);
                _immortal = false;
            });
        }

        private void OnGameStateChanged(GameController.GameState gameState)
        {
            if (gameState >= GameController.GameState.Finish)
            {
                _finishImmortal = true;
            }
            else
            {
                _finishImmortal = false;
            }
        }

        private void OnDestroy()
        {
            GameController.GameController.Instance.OnGameStateChanged -= OnGameStateChanged;
        }
    }
}
