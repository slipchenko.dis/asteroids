﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroids
{
    public class JuniorAsteroid : Asteroid
    {
        void Start()
        {
            AsteroidSize = AsteroidSize.Junior;
            _rigidbody = GetComponent<Rigidbody2D>();
            SetVelocity();
        }

        protected override void DestroyAndSpawn()
        {
            Destroy();
        }
    }
}