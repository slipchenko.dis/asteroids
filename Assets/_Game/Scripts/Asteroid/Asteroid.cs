﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Asteroids
{
    public enum AsteroidSize
    {
        Junior,
        Middle,
        Senior
    }

    [RequireComponent(typeof(Rigidbody2D))]
    public abstract class Asteroid : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private float _maxForwardSpeed;
        [SerializeField] private float _minForwardSpeed;
        [SerializeField] private int _scoreAmountPerDestroy;
        [SerializeField] protected int _spawnedAsteroidsCount;
        [SerializeField] protected GameObject _spawnedAsteroid;
        [SerializeField] protected GameObject _destroyEffect;

        protected AsteroidSize AsteroidSize { get; set; }

        protected Rigidbody2D _rigidbody;

        private void OnEnable()
        {
            GameController.Spawner.Instance.AddAsteroidToList(this);
        }

        protected void SetVelocity()
        {
            var speed = Random.Range(_minForwardSpeed, _maxForwardSpeed);
            var direction = new Vector2(Random.Range(0, 1f), Random.Range(0, 1f));

            _rigidbody.velocity = direction * speed;
        }

        protected void Destroy()
        {
            GameController.GameController.Instance.PlayerScore += _scoreAmountPerDestroy;

            if (_destroyEffect)
            {
                Instantiate(_destroyEffect, transform.position, Quaternion.identity);
            }

            GameController.Spawner.Instance.RequestSpawnEmeny();

            Destroy(gameObject);
        }

        abstract protected void DestroyAndSpawn();


        private void OnTriggerEnter2D(Collider2D collision)
        {
            OnTrigger(collision);
        }

        protected void OnTrigger(Collider2D collision)
        {
            var bullet = collision.GetComponent<Bullet.Bullet>();

            if (bullet && bullet.BulletType == Bullet.BulletType.PlayerBullet)
            {
                Destroy(bullet);
                GameController.Spawner.Instance.RemoveAsteroidFromList(this);
                DestroyAndSpawn();
            }
        }
    }
}