﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroids
{
    public class MiddleAsteroid : Asteroid
    {
        void Start()
        {
            AsteroidSize = AsteroidSize.Middle;
            _rigidbody = GetComponent<Rigidbody2D>();
            SetVelocity();
        }

        protected override void DestroyAndSpawn()
        {
            Destroy();

            if (_spawnedAsteroid)
            {
                for (int i = 0; i < _spawnedAsteroidsCount; i++)
                {
                    Instantiate(_spawnedAsteroid, transform.position, Quaternion.identity);
                }
            }
        }
    }
}
