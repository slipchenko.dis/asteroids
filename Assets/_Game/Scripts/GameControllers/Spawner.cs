﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameController
{
    public class Spawner : MonoBehaviour
    {
        #region Singleton
        public static Spawner Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        private const int _defaultAsteroidAmount = 4;
        private const int _valueToSpawnSimpleEnemy = 20;
        private const int _valueToSpawnSmartEnemy = 15;

        [SerializeField] private GameObject _asteroid;
        [SerializeField] private GameObject _simpleEnemy;
        [SerializeField] private GameObject _smartEnemy;

        public List<GameObject> _spawnedAsteroids;

        public List<GameObject> _spawnedEnemy;

        private void Start()
        {
            _spawnedAsteroids = new List<GameObject>();
            _spawnedEnemy = new List<GameObject>();

            GameController.Instance.OnGameStateChanged += OnGameStateChanged;
            OnGameStateChanged(GameController.Instance.GameState);
        }

        private void OnDestroy()
        {
            GameController.Instance.OnGameStateChanged -= OnGameStateChanged;
        }

        public void AddAsteroidToList(Asteroids.Asteroid asteroid)
        {
            _spawnedAsteroids.Add(asteroid.gameObject);
        }

        public void AddEnemyToList(Enemy.Enemy enemy)
        {
            _spawnedEnemy.Add(enemy.gameObject);
        }

        public void RemoveAsteroidFromList(Asteroids.Asteroid asteroid)
        {
            _spawnedAsteroids.Remove(asteroid.gameObject);

            GameController.Instance.CheckWinState(_spawnedAsteroids.Count, _spawnedEnemy.Count);
        }

        public void RemoveEnemyFromList(Enemy.Enemy enemy)
        {
            _spawnedEnemy.Remove(enemy.gameObject);

            GameController.Instance.CheckWinState(_spawnedAsteroids.Count, _spawnedEnemy.Count);
        }

        private void OnGameStateChanged(GameState gameState)
        {
            switch (gameState)
            {
                case GameState.WaitingForInput:
                    break;
                case GameState.Pause:
                    break;
                case GameState.Lose:
                    break;
                case GameState.Win:
                    break;
                case GameState.Finish:
                    break;
                case GameState.GamePlay:
                    var asteroidsAmount = CalculateAsteroidsOnLevel(GameController.Instance.Level);
                    SpawnAsteroids(asteroidsAmount);
                    break;
            }
        }

        private int CalculateAsteroidsOnLevel(int level)
        {
            var asteroidsAmount = _defaultAsteroidAmount + level - 1;

            return asteroidsAmount;
        }

        private void SpawnAsteroids(int asteroidsAmount)
        {
            for (int i = 0; i < asteroidsAmount; i++)
            {
                var randomPos = GetRandomPosition();

                Instantiate(_asteroid, randomPos, Quaternion.identity);
            }
        }

        private Vector2 GetRandomPosition()
        {
            var screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

            var randomPos = new Vector2(Random.Range(0, screenBounds.x), Random.Range(0, screenBounds.y));

            return randomPos;
        }

        public void RequestSpawnEmeny()
        {
            var spawnSimpleNLO = ChanceSpawnEnemy(GameController.Instance.Level, _valueToSpawnSimpleEnemy);

            if (spawnSimpleNLO)
            {
                var spawnSmartNLO = ChanceSpawnEnemy(GameController.Instance.Level, _valueToSpawnSmartEnemy);

                if (spawnSmartNLO)
                {
                    var randomPos = GetRandomPosition();

                    Instantiate(_smartEnemy, randomPos, Quaternion.identity);

                }
                else
                {
                    var randomPos = GetRandomPosition();

                    Instantiate(_simpleEnemy, randomPos, Quaternion.identity);
                }
            }
        }

        private bool ChanceSpawnEnemy(int level, int maxValue)
        {
            var spawn = false;

            var minValue = Mathf.Clamp(level, 1, 10);

            var n = Random.Range(minValue, maxValue + 1);

            if (n == maxValue)
            {
                spawn = true;
            }

            return spawn;
        }
    }
}
