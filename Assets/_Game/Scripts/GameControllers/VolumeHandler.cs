﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameController
{
    public class VolumeHandler : MonoBehaviour
    {
        #region Singleton
        public static VolumeHandler Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion


        [SerializeField] private AudioSource _myFx;
        [SerializeField] private AudioClip _laser;
        [SerializeField] private AudioClip _explosion;

        public bool Volume
        {
            get
            {
                return Convert.ToBoolean(PlayerPrefs.GetInt("isVolume", 1));
            }
            set
            {
                PlayerPrefs.SetInt("isVolume", Convert.ToInt32(value));
            }
        }

        public void VolumeLaser()
        {
            if (Volume)
            {
                _myFx.PlayOneShot(_laser);
            }
        }

        public void VolumeExplosion()
        {
            if (Volume)
            {
                _myFx.PlayOneShot(_explosion);
            }
        }
    }
}
