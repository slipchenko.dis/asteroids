﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameController
{
    public class CameraController : MonoBehaviour
    {
        #region Singleton
        public static CameraController Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        [SerializeField] private Animator _animator;

        public void Shake()
        {
            _animator.SetTrigger("Shake");
        }
    }
}
