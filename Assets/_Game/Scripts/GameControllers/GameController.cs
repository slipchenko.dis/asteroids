﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameController
{
    public enum GameState
    {
        WaitingForInput,
        GamePlay,
        Pause,
        Finish,
        Lose,
        Win
    }

    public class GameController : MonoBehaviour
    {
        #region Singleton
        public static GameController Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        public Action<int> OnPlayerScoreChanged;

        public int PlayerScore
        {
            get
            {
                return _score;
            }
            set
            {
                _score = value;
                OnPlayerScoreChanged?.Invoke(_score);
            }
        }

        private int _score;

        public event Action<GameState> OnGameStateChanged;
        [SerializeField] private GameState _gameState;

        private void Start()
        {
            GameState = GameState.GamePlay;
        }

        public int Level
        {
            get => PlayerPrefs.GetInt("Level", 1);
            set => PlayerPrefs.SetInt("Level", value);
        }


        public GameState GameState
        {
            get
            {
                return _gameState;
            }
            set
            {
                _gameState = value;
                OnGameStateChanged?.Invoke(_gameState);

                switch (_gameState)
                {
                    case GameState.Win:
                        Level++;
                        break;
                    case GameState.Finish:
                        break;
                    case GameState.Lose:
                        break;
                    case GameState.WaitingForInput:
                        break;
                }
            }
        }

        public bool CheckWinState(int asteroidAmount, int enemyAmount)
        {
            if (asteroidAmount <= 0 && enemyAmount <= 0)
            {
                GameState = GameState.Win;
                return true;
            }

            return false;
        }
    }
}