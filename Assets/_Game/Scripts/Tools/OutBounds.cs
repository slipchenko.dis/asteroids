﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tools
{
    public class OutBounds : MonoBehaviour
    {
        private Vector2 _screenBounds;

        void Start()
        {
            _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        }

        void Update()
        {
            Vector3 objPos = transform.position;

            if (objPos.x > _screenBounds.x)
            {
                TeleportPlayerToOtherSide("horizontal");
            }
            if (objPos.x < _screenBounds.x * -1)
            {
                TeleportPlayerToOtherSide("horizontal");
            }
            if (objPos.y > _screenBounds.y)
            {
                TeleportPlayerToOtherSide("vertical");
            }
            if (objPos.y < _screenBounds.y * -1)
            {
                TeleportPlayerToOtherSide("vertical");
            }
        }

        private void TeleportPlayerToOtherSide(string bound)
        {
            var pos = transform.position;

            switch (bound)
            {
                case "horizontal":
                    pos.x = Camera.main.transform.position.x - pos.x;
                    break;
                case "vertical":
                    pos.y = Camera.main.transform.position.y - pos.y;
                    break;
            }

            pos.x = Mathf.Clamp(pos.x, _screenBounds.x * -1, _screenBounds.x);
            pos.y = Mathf.Clamp(pos.y, _screenBounds.y * -1, _screenBounds.y);
            pos.z = 0;
            transform.position = pos;
        }
    }
}
