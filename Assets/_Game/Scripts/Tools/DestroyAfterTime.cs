﻿using UnityEngine;

namespace Tools
{
    public class DestroyAfterTime : MonoBehaviour
    {
        [SerializeField] private float _destroyTime;

        private void Start()
        {
            Invoke("DestroySelf", _destroyTime);
        }

        void DestroySelf()
        {
            Destroy(gameObject);
        }
    }
}
