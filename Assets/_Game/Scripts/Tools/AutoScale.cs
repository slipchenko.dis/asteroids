﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Tools
{
    public class AutoScale : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private float _animationSpeed = 1f;
        [SerializeField] private float _minScale = 0.3f;
        [SerializeField] private Ease _ease = Ease.InBounce;

        private float _startScale;

        private void Start()
        {
            _startScale = transform.localScale.x;
            Scale();
        }

        private void Scale()
        {
            transform.DOScale(_minScale, _animationSpeed).SetEase(_ease).OnComplete(() =>
            {
                transform.DOScale(_startScale, _animationSpeed).SetEase(_ease).OnComplete(() =>
                {
                    Scale();
                });
            });
        }
    }
}
